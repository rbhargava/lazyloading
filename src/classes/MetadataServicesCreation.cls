public with sharing class MetadataServicesCreation {
    
 public static void createObject()
    {
        MetadataService.MetadataPort service = createService();
        MetadataService.CustomObject customObject = new MetadataService.CustomObject();
        customObject.fullName = 'NewObject__c';
        customObject.label = 'New Object';
        customObject.pluralLabel = 'Tests';
        customObject.nameField = new MetadataService.CustomField();
        customObject.nameField.type_x = 'Text';
        customObject.nameField.label = 'Test Record';
        customObject.deploymentStatus = 'Deployed';
        customObject.sharingModel = 'ReadWrite';
        List<MetadataService.SaveResult> results =      
            service.createMetadata(
                new MetadataService.Metadata[] { customObject });       
        handleSaveResults(results[0]);
    }

    public static void upsertObject()
    {
        MetadataService.MetadataPort service = createService();
        MetadataService.CustomObject customObject = new MetadataService.CustomObject();
        customObject.fullName = 'Test__c';
        customObject.label = 'Test';
        customObject.pluralLabel = 'Tests Upsert';
        customObject.nameField = new MetadataService.CustomField();
        customObject.nameField.type_x = 'Text';
        customObject.nameField.label = 'Test Record Upsert';
        customObject.deploymentStatus = 'Deployed';
        customObject.sharingModel = 'ReadWrite';
        List<MetadataService.UpsertResult> results =        
            service.upsertMetadata(
                new MetadataService.Metadata[] { customObject });       
        handleUpsertResults(results[0]);
    }
    public static void createField()
    {
        MetadataService.MetadataPort service = createService();     
        MetadataService.CustomField customField = new MetadataService.CustomField();
        customField.fullName = 'NewObject__c.TestField__c';
        customField.label = 'Test Field';
        customField.type_x = 'Text';
        customField.length = 42;
        List<MetadataService.SaveResult> results =      
            service.createMetadata(
                new MetadataService.Metadata[] { customField });                
        handleSaveResults(results[0]);
    }
    
    public static void updateField()
    {
        MetadataService.MetadataPort service = createService(); 
        MetadataService.CustomField customField = new MetadataService.CustomField();
        customField.fullName = 'Test__c.TestField__c';
        customField.label='New Test Field Label';
        customField.type_x = 'Text'; 
        customField.length = 52;
        List<MetadataService.SaveResult> results =      
            service.updateMetadata(
                new MetadataService.Metadata[] { customField });                
        handleSaveResults(results[0]);      
    }
    
    public static void dynamicCreation(String objectName,List<String> fieldNames)
    {
        //System.assertEquals(null,fieldNames[5]);
        // Define Metadata item to create a Custom Object
        MetadataService.CustomObject customObject = new MetadataService.CustomObject();
        customObject.fullName = objectName.replace(' ','_')+'__c';
        customObject.label = objectName;
        customObject.pluralLabel = objectName+'s';
        customObject.nameField = new MetadataService.CustomField();
        customObject.nameField.type_x = 'Text';
        customObject.nameField.label = 'Name Field Default';
        customObject.deploymentStatus = 'Deployed';
        customObject.sharingModel = 'ReadWrite';
        
        
      /* List<MetadataService.Metadata> metaDataList = new List<MetadataService.Metadata>();
       //for(Integer i=0;i<fieldNames.size();i++){
        System.debug('----------SIZE---------------'+fieldNames.size());
        for(String str:fieldNames){
            MetadataService.CustomField customField1 = new MetadataService.CustomField();
            customField1.fullName = objectName.replace(' ','_')+'__c.'+str.replace(' ','_')+'__c';
            customField1.label = str;
            customField1.type_x = 'Text';
            customField1.length = 42;
            metaDataList.add((MetadataService.Metadata)customField1);
        }
        System.debug('----------Meta data List---------------'+metaDataList);
       //}*/
       
       
       
        
        /* Define Metadata item to create a Custom Field on the above object
        MetadataService.CustomField customField1 = new MetadataService.CustomField();
        customField1.fullName = objectName+'__c.'+fieldNames[0]+'__c';
        customField1.label = fieldNames[0];
        customField1.type_x = 'Text';
        customField1.length = 42;

        // Define Metadata item to create a Custom Field on the above object
        MetadataService.CustomField customField2 = new MetadataService.CustomField();
        customField2.fullName = objectName+'__c.'+fieldNames[1]+'__c';
        customField2.label = fieldNames[1];
        customField2.type_x = 'Text';
        customField2.length = 42;*/
        
        // Create components in the correct order
        MetadataService.MetadataPort service = createService();
        List<MetadataService.SaveResult> results =      
            service.createMetadata(
                new MetadataService.Metadata[] { customObject });       
        //handleSaveResults(results[0]);
        System.Debug('***********BEFORE ENTERING BATCH******');
       Id jobId = Database.executeBatch(new BatchCreateMetadataSoap(objectName,fieldNames,service), 10);
        //results = service.createMetadata(metaDataList);     
        //handleSaveResults(results[0]);              
        //handleSaveResults(results[1]);   
         System.Debug('***********AFTER ENTERING BATCH******');       
    }
    
    public static MetadataService.MetadataPort createService()
    { 
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;     
    }

    /**
     * Example helper method to interpret a SaveResult, throws an exception if errors are found
     **/
    private static void handleSaveResults(MetadataService.SaveResult saveResult)
    {
        // Nothing to see?
        if(saveResult==null || saveResult.success)
            return;
        // Construct error message and throw an exception
        List<String> messages = new List<String>();
        messages.add(
            (saveResult.errors.size()==1 ? 'Error ' : 'Errors ') + 
                'occured processing component ' + saveResult.fullName + '.');
        for(MetadataService.Error error : saveResult.errors)
            messages.add(
                error.message + ' (' + error.statusCode + ').' + 
                ( error.fields!=null && error.fields.size()>0 ? 
                    ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
        if(messages.size()>0)
            throw new MetadataServiceExamplesException(String.join(messages, ' '));
    }   
    private static void handleUpsertResults(MetadataService.UpsertResult upsertResult)
    {
        // Nothing to see?
        if(upsertResult==null || upsertResult.success)
            return;
        // Construct error message and throw an exception
        List<String> messages = new List<String>();
        messages.add(
            (upsertResult.errors.size()==1 ? 'Error ' : 'Errors ') + 
                'occured processing component ' + upsertResult.fullName + '.');
        for(MetadataService.Error error : upsertResult.errors)
            messages.add(
                error.message + ' (' + error.statusCode + ').' + 
                ( error.fields!=null && error.fields.size()>0 ? 
                    ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
        if(messages.size()>0)
            throw new MetadataServiceExamplesException(String.join(messages, ' '));
    }
    public class MetadataServiceExamplesException extends Exception { }
}
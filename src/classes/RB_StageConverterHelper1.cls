global class RB_StageConverterHelper1{

    global Map <String, Schema.SObjectField> getFieldMap(String objectName){
        
        Map<String, Schema.SObjectType> globldes; //Map - Name to Schema of SObjects.
        try{
            globldes = Schema.getGlobalDescribe(); //Fills the map of Object Name to Schema of each Object.
        }
        catch(Exception e){
        }
        Map <String, Schema.SObjectField> fieldMap = globldes.get(objectName).getDescribe().fields.getMap();
        return fieldMap;
    }
    global String createQuery(String objectName,Map <String, Schema.SObjectField> fieldMap){
        
        String RecordQuery= 'select ';
        for(String nm:fieldMap.keyset()){
            if(RecordQuery=='select '){
                RecordQuery= RecordQuery+nm;
            }
            else{
                RecordQuery= RecordQuery+', '+nm;
            }
        }
        RecordQuery= RecordQuery+' from '+objectName;
        return RecordQuery;
    }

}
global with sharing class BatchCreateMetadataSoap implements Database.Batchable<String>,Database.AllowsCallouts{
    
    public List<String> fieldNames;
    public String ObjectName;
    public MetadataService.MetadataPort service;
     List<MetadataService.Metadata> metaDataList = new List<MetadataService.Metadata>();
    public BatchCreateMetadataSoap(String ObjectName,List<String> fieldNames,MetadataService.MetadataPort service){
        this.fieldNames = fieldNames;
        this.ObjectName = ObjectName;
        this.service = service;
    }
    
    global Iterable<String> start(Database.BatchableContext BC)
    {
        return fieldNames;
        //return new List<String> { 'Do something', 'Do something else', 'And something more' };
    }
    
    global void execute(Database.BatchableContext BC, List<String> scope)
    {
         //List<MetadataService.Metadata> metaDataList = new List<MetadataService.Metadata>();
         System.debug('----------Scope Size---------------'+scope.size());
         System.debug('----------Scope---------------'+scope);
         System.debug('----------metaDataList Size---------------'+scope.size());
        for(String str:scope){
            MetadataService.CustomField customField1 = new MetadataService.CustomField();
            customField1.fullName = objectName.replace(' ','_').trim()+'__c.'+str.replace(' ','_').trim()+'__c';
            customField1.label = str;
            customField1.type_x = 'Text';
            customField1.length = 42;
            metaDataList.add((MetadataService.Metadata)customField1);
        }
        
      /*  MetadataService.CustomField customField2 = new MetadataService.CustomField();
            System.Debug('**** Point of verification begin *****');
            customField2.fullName = objectName.replace(' ','_').trim()+'__c.'+'isError'+'__c';
            customField2.label = 'isError';
            customField2.type_x = 'Checkbox';
            customField2.defaultValue = 'false';
             System.Debug('**** Point of verification end *****');
            metaDataList.add((MetadataService.Metadata)customField2); */
            
        /*MetadataService.CustomField customField3 = new MetadataService.CustomField();
            customField3.fullName = objectName.replace(' ','_').trim()+'__c.'+'Error_Message'+'__c';
            customField3.label = 'Error Message';
            customField3.type_x = 'Checkbox';
           
            metaDataList.add((MetadataService.Metadata)customField3);*/
            
        
        //MetadataService.MetadataPort service = MetadataServicesCreation.createService();
        List<MetadataService.SaveResult> results = service.createMetadata(metaDataList); 
        
        System.debug('----------Meta data List---------------'+metaDataList);
    }
       
    global void finish(Database.BatchableContext BC)
    {
         MetadataService.CustomField customField2 = new MetadataService.CustomField();
            System.Debug('**** Point of verification begin *****');
            customField2.fullName = objectName.replace(' ','_').trim()+'__c.'+'isError'+'__c';
            customField2.label = 'isError';
            customField2.type_x = 'Checkbox';
            customField2.defaultValue = 'false';
            System.Debug('**** Point of verification end *****');
            metaDataList.add((MetadataService.Metadata)customField2);
            
         MetadataService.CustomField customField4 = new MetadataService.CustomField();
            System.Debug('**** Point of verification begin *****');
            customField4.fullName = objectName.replace(' ','_').trim()+'__c.'+'Processed'+'__c';
            customField4.label = 'Processed';
            customField4.type_x = 'Checkbox';
            customField4.defaultValue = 'false';
            System.Debug('**** Point of verification end *****');
            metaDataList.add((MetadataService.Metadata)customField4);
            
            MetadataService.CustomField customField3 = new MetadataService.CustomField();
            customField3.fullName = objectName.replace(' ','_').trim()+'__c.'+'Error_Message'+'__c';
            customField3.label = 'Error Message';
            customField3.type_x = 'Html';
            customField3.length = 32768;
            customField3.visibleLines= 10;
            metaDataList.add((MetadataService.Metadata)customField3);
            List<MetadataService.SaveResult> results = service.createMetadata(metaDataList); 
    
    }
    
    

}
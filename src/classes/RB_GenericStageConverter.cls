global class RB_GenericStageConverter implements Database.Batchable<sObject>{
    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       BATCH CLASS
       Purpose - Move Data from a Staging Object to Actual Object. Class needs to be Generic and could be
           used in any Org.
       Created By - Rachit Bhargava
       Created Date - 10/09/2014
       Last Modified date 20/02/2015
       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */ 
    
    //
    global String stageObjectName;
    //Constructor
    global RB_GenericStageConverter(String name){
    stageObjectName = name;
    }
    
    
    //Batch Class start Method to get all the records from Staging Object.
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        String stagingRecordQuery;
        RB_StageConverterHelper1 helperInstance = new RB_StageConverterHelper1();
        Map <String, Schema.SObjectField> fieldMap = helperInstance.getFieldMap(stageObjectName);
        stagingRecordQuery= helperInstance.createQuery(stageObjectName,fieldMap)+ ' where IsProcessed__c=false';
        System.debug('Staging Query :: '+stagingRecordQuery);        
        return Database.getQueryLocator(stagingRecordQuery);
    }
    global void execute(Database.BatchableContext bc, List<SObject> listOfStagingRecords){
        
        Boolean error; 
        String mapRecordQuer;
        RB_StageConverterHelper1 helperInstance = new RB_StageConverterHelper1();
        Map <String, Schema.SObjectField> fieldMap = helperInstance.getFieldMap('Mapping__c');
        mapRecordQuer= helperInstance.createQuery('Mapping__c',fieldMap)+ ' where Name=\''+stageObjectName+'\' order by Sequence__c asc';
        System.debug('Map Query :: '+mapRecordQuer);
        
        List<Sobject> finalList = new List<SObject>();
        Map<String,Mapping__c> stgFieldNames = new Map<String,Mapping__c>();
        List<Mapping__c> currentMap = Database.query(mapRecordQuer);
        String targetObjName;
        for(Mapping__c mpIns:currentMap){
           stgFieldNames.put(mpIns.Stage_Object_Field_Name__c,mpIns);
           targetObjName= mpIns.Target_Object__c;
        }
        String errorM;
        helperInstance = new RB_StageConverterHelper1();
        Map <String, Schema.SObjectField> actStgObjfieldMap = helperInstance.getFieldMap(targetObjName);
        for(Sobject actSObj:listOfStagingRecords){
            errorM='';
            actSObj.put('IsError__c',false);
            actSObj.put('Error_Message__c','');
            sObject actObj = Schema.getGlobalDescribe().get(targetObjName).newSObject();
            for(String nm:stgFieldNames.keyset()){
                try{
                    Schema.DescribeFieldResult F = actStgObjfieldMap.get(stgFieldNames.get(nm).Target_Object_Field_Name__c).getDescribe();
                    String type = String.valueOf(F.getType());
                    System.debug(type+' '+nm);
                    if(!F.isNillable() && actSObj.get(nm)==null){
                        
                        
                        if(type=='BOOLEAN'){
                          System.debug('Rachit'+nm);   
                          if(actSObj.get(nm)==''){
                              System.debug('in for NUll');
                              actObj.put(stgFieldNames.get(nm).Target_Object_Field_Name__c,Boolean.valueOf(String.valueOf('false')));
                          }
                        }
                        else{
                        actSObj.put('IsError__c',true);
                        errorM = errorM+'-'+'Required Field '+nm+' is has no value'+' <br/>';
                        }
                               
                    }
                    else{
                        if(type=='DATE'){
                            if(String.isNotEmpty(String.valueOf(actSObj.get(nm))) || String.isNotEmpty(String.valueOf(actSObj.get(nm)))){
                            Date d = date.parse(String.valueOf(actSObj.get(nm)));
                            actObj.put(stgFieldNames.get(nm).Target_Object_Field_Name__c, d);//mapValsAccDataType(actObj,mapWrap,nm,actObjField);
                            }
                        }
                        else if(type=='DOUBLE'){
                           if(String.isNotEmpty(String.valueOf(actSObj.get(nm))) || String.isNotEmpty(String.valueOf(actSObj.get(nm)))){
                            string strData = String.valueOf(actSObj.get(nm));
                            string strNm = strData.replaceAll('[A-Z a-z $ , .]', '');
                            System.debug(stgFieldNames.get(nm).Target_Object_Field_Name__c);
                            actObj.put(stgFieldNames.get(nm).Target_Object_Field_Name__c,Decimal.valueOf(strNm));
                            }
                        }
                        else if(type=='BOOLEAN'){
                              System.debug('in for NOT Null');
                              actObj.put(stgFieldNames.get(nm).Target_Object_Field_Name__c,Boolean.valueOf(String.valueOf(actSObj.get(nm))));
                          }
                          
                        else if(type=='CURRENCY'){
                            if(String.isNotEmpty(String.valueOf(actSObj.get(nm))) || String.isNotEmpty(String.valueOf(actSObj.get(nm)))){
                            string strData = String.valueOf(actSObj.get(nm));
                            string strNm = strData.replaceAll('[A-Z a-z $ , .]', '');
                            System.debug(stgFieldNames.get(nm).Target_Object_Field_Name__c);
                            actObj.put(stgFieldNames.get(nm).Target_Object_Field_Name__c,Decimal.valueOf(strNm));
                           } 
                        } 
                        else{
                            if(actSObj.get(nm)!=null || actSObj.get(nm)!=''){
                            actObj.put(stgFieldNames.get(nm).Target_Object_Field_Name__c,actSObj.get(nm));
                            }
                        }
                    }
                }
                catch(Exception e){
                actSObj.put('IsError__c',true);
                Schema.DescribeFieldResult F = actStgObjfieldMap.get(stgFieldNames.get(nm).Target_Object_Field_Name__c).getDescribe();
                String type = String.valueOf(F.getType());
                errorM = errorM+'-'+e.getMessage()+' '+nm+' '+e.getlinenumber()+'<br/>';
                System.debug(e.getMessage()+' ############ '+nm+'Nm TYPE *******'+type);
                }
            }
            System.debug('SObject Added To List ::::: '+actObj);
            if(Boolean.valueOf(actSObj.get('IsError__c'))){
                actSObj.put('Error_Message__c',errorM);
            }
            else{
                actSObj.put('IsProcessed__c',true);
                finalList.add(actObj);
            }
            
        }
        database.update(listOfStagingRecords);
        database.insert(finalList,false);//database.upsert(finalList,<externalFieldIdName>,false);
    }
    
    
    global void finish(Database.BatchableContext bc){
    }
}
global class FileUploaderNew
{
    @RemoteAction
    global static Boolean readFileFromJavascript(String objectNameFromJS,List<String>fieldsList){
        System.debug('*****'+objectNameFromJS);
        System.debug('**fieldsList***'+fieldsList);
        MetadataServicesCreation.dynamicCreation(objectNameFromJS,fieldsList);
        return true;
    }
             
}
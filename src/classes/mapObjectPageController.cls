global class mapObjectPageController {
    
    public static final Map<String,String> mapObjectlabelsAndAPI=new Map<String,String>();
    
    
    @RemoteAction   
    global static List<String> getStgObjectItems(){
        List<String> options = new List<String>();
        Map<String, Schema.SObjectType> gd;
        try {
            gd = Schema.getGlobalDescribe();
        }
        catch(Exception e){
        }
        for (Schema.SObjectType sobj:gd.values()){
            String labels = sobj.getDescribe().getLabel();
            String names = sobj.getDescribe().getName();
           // mapObjectlabelsAndAPI.put(labels,names);  
            if(names.contains('Stage')){
                options.add(labels+'('+names+')');
            }
        }
        options.sort();
        return options;           
    }
    @RemoteAction   
    global static List<String> getTarObjectItems(){
        List<String> options = new List<String>();
        Map<String, Schema.SObjectType> gd;
        try {
            gd = Schema.getGlobalDescribe();
        }
        catch(Exception e){
        }
        for (Schema.SObjectType sobj:gd.values()){
            String labels = sobj.getDescribe().getLabel();
            String names = sobj.getDescribe().getName();
           // mapObjectlabelsAndAPI.put(labels,names);  
            
                options.add(labels+'('+names+')');
            
        }
        options.sort();
        return options;           
    }
    @RemoteAction   
    global static List<String> getTarChildObject(String parentObject){
    Set<String> options = new Set<String>();
    List<String> optionList = new List<String>();
        Map<String, Schema.SObjectType> gd;
        if (parentObject!= NULL) {
            try {
                gd = Schema.getGlobalDescribe();
            }
            catch(Exception e){
            }
            
            String parentAPIval = parentObject.substring(parentObject.indexOf('(')+1,parentObject.indexOf(')'));
            
            List<Schema.ChildRelationship> childs = gd.get(parentAPIval).getDescribe().getChildRelationships();
            for(Schema.ChildRelationship iter:childs){
                Schema.SObjectType s  = iter.getChildSObject();
                String labels = s.getDescribe().getLabel();
                String names = s.getDescribe().getName();
               
                options.add(labels+'('+names+')'+':'+iter.getField().getDescribe().getName());
            }
        }
        for(String iter:options){
            optionList.add(iter);
        }
        optionList.sort();
        return optionList;  
    }    
    @RemoteAction   
    global static List<String> getStgFields(String stgObjAPI, String stgObjname){
        
       
        List<String> fieldNames = new List<String>();
        Map<String, Schema.SObjectType> gd;
        try {
            gd = Schema.getGlobalDescribe();
        }
        catch(Exception e){
        }
        //System.assertEquals
        //String stgObjAPI= ObjName.substring(ObjName.indexOf('(')+1,ObjName.indexOf(')'));
        //String stgObjname= ObjName.substring(0,ObjName.indexOf('('));
        Map <String, Schema.SObjectField> fieldMap = gd.get(stgObjAPI).getDescribe().fields.getMap();
        for(String nm:fieldMap.keyset()){
            if(!fieldMap.get(nm).getDescribe().isDefaultedOnCreate()&& !fieldMap.get(nm).getDescribe().isAutoNumber() && !fieldMap.get(nm).getDescribe().isCalculated()){// && fieldMap.get(nm).getDescribe().isUpdateable() ){
                String label = fieldMap.get(nm).getDescribe().getLabel();
                fieldNames.add(stgObjname+': '+label);
            }
            else if(fieldMap.get(nm).getDescribe().isDefaultedOnCreate() && fieldMap.get(nm).getDescribe().isNameField() )
            {
                String label = fieldMap.get(nm).getDescribe().getLabel();
                fieldNames.add(stgObjname+': '+label);
            }
        }
        fieldNames.sort();
        return fieldNames;
    }
    @RemoteAction   
    global static List<String> getTarFields(List<String> tObjNames){
        List<String> fieldNames = new List<String>();
        Map<String, Schema.SObjectType> gd;
        try {
            gd = Schema.getGlobalDescribe();
        }
        catch(Exception e){
        }
        for(String ObjName:tObjNames){
            String stgObjAPI= ObjName.substring(ObjName.indexOf('(')+1,ObjName.indexOf(')'));
            String stgObjname= ObjName.substring(0,ObjName.indexOf('('));
            Map <String, Schema.SObjectField> fieldMap = gd.get(stgObjAPI).getDescribe().fields.getMap();
            for(String nm:fieldMap.keyset()){
                if(!fieldMap.get(nm).getDescribe().isDefaultedOnCreate()&& !fieldMap.get(nm).getDescribe().isAutoNumber() && !fieldMap.get(nm).getDescribe().isCalculated() && String.valueOf(fieldMap.get(nm).getDescribe().getType())!='Reference'){// && fieldMap.get(nm).getDescribe().isUpdateable()){
                    String label = fieldMap.get(nm).getDescribe().getLabel();
                    fieldNames.add(stgObjname+': '+label);
                }
                else if(fieldMap.get(nm).getDescribe().isDefaultedOnCreate() && fieldMap.get(nm).getDescribe().isNameField() )
                {
                    String label = fieldMap.get(nm).getDescribe().getLabel();
                    fieldNames.add(stgObjname+': '+label);
                }
            }
        }
        fieldNames.sort();
        return fieldNames;
    }    
    
    @RemoteAction
    global static List<String> createAutoMatchedSeq(List<String> stgList, List<String> tarList){
        Integer rowCounter=0;
        List<String> newList = new List<String>();
        //System.assertEquals(tarList,null);
        for(String iter1 : stgList){
            if(iter1!=''){
                rowCounter++;
                System.debug(iter1);
                String fldName= iter1.substring(iter1.indexOf(':'),iter1.length());
                Boolean flag =false;
                for(String iter2 : tarList){
                    if(iter2!=''){
                        
                        String fldName2= iter2.substring(iter2.indexOf(':'),iter2.length());
                        if(fldName2==fldName){
                            newList.add(iter2);
                            flag = true;
                        }
                    }
                }
                if(!flag){
                    newList.add('');
                }
            }
        }
        return newList;
    }
    @RemoteAction   
    global static List<String> getPopFields(String ObjName,List<String> ignoreList){
        List<String> fieldNames = new List<String>();
        Map<String, Schema.SObjectType> gd;
        try {
            gd = Schema.getGlobalDescribe();
        }
        catch(Exception e){
        }
        //System.assertEquals(ignoreList,null);
        String stgObjAPI= ObjName.substring(ObjName.indexOf('(')+1,ObjName.indexOf(')'));
        String stgObjname= ObjName.substring(0,ObjName.indexOf('('));
        Map <String, Schema.SObjectField> fieldMap = gd.get(stgObjAPI).getDescribe().fields.getMap();
        for(String nm:fieldMap.keyset()){
            if(!fieldMap.get(nm).getDescribe().isDefaultedOnCreate()&& !fieldMap.get(nm).getDescribe().isAutoNumber() && !fieldMap.get(nm).getDescribe().isCalculated() && String.valueOf(fieldMap.get(nm).getDescribe().getType())!='Reference'){// && fieldMap.get(nm).getDescribe().isUpdateable()){
                String label = fieldMap.get(nm).getDescribe().getLabel();
                String popLabel = stgObjname+': '+label;
                Boolean flag=true;
                for(String iter:ignoreList){
                    if(iter.contains(popLabel)){
                        System.debug('tableVal: '+iter+'PickVal: '+popLabel);
                        flag=false;
                    }
                }
                if(flag){
                    fieldNames.add(popLabel);
                }
            }
            else if(fieldMap.get(nm).getDescribe().isDefaultedOnCreate() && fieldMap.get(nm).getDescribe().isNameField() )
            {
                String label = fieldMap.get(nm).getDescribe().getLabel();
                String popLabel = stgObjname+': '+label;
                Boolean flag=true;
                for(String iter:ignoreList){
                    if(iter.contains(popLabel)){
                        System.debug('tableVal: '+iter+'PickVal: '+popLabel);
                        flag=false;
                    }
                }
                if(flag){
                    fieldNames.add(popLabel);
                }
            }
        }
        fieldNames.sort();
        return fieldNames;
    }
    @RemoteAction   
    global static void save(String stgAPIName,List<String> objList,List<String> stgFieldList,List<String> tarFieldList){
        Map<String,String> maplabelName = new Map<String,String>();
        Map<String,Integer> maplabelSequence = new Map<String,Integer>();
        List<Mapping__c> listOfMapping = new List<Mapping__c>();
        Map<String, Schema.SObjectType> gd;
        try {
            gd = Schema.getGlobalDescribe();
        }
        catch(Exception e){
        }
        for(Integer i=0;i<objList.size();i++){
            String ObjAPI= objList[i].substring(objList[i].indexOf('(')+1,objList[i].indexOf(')'));
            String Objname= objList[i].substring(0,objList[i].indexOf('('));
            maplabelName.put(Objname,ObjAPI);
            maplabelSequence.put(Objname,i+1);
            if(i>0){
                Mapping__c inst = new Mapping__c();
                inst.Name = stgAPIName;
                
                inst.Target_Object__c = ObjAPI;
                inst.Target_Object_Field_Name__c = objList[i].split(':')[1];
                inst.Sequence__c = i+1;
                inst.isLookup__c = true;
                inst.Lookup_Field_Name__c = objList[i-1].substring(objList[i-1].indexOf('(')+1,objList[i-1].indexOf(')'))+'.Id';
                listOfMapping.add(inst);
            }
        }
        
        Map <String, Schema.SObjectField> stgfieldMap = gd.get(stgAPIName).getDescribe().fields.getMap();
        for(Integer i=0;i<tarFieldList.size()-1;i++){
            //System.assertEquals(null,'x'+tarFieldList[i].trim()+'y');
            if(tarFieldList[i].trim()!=' '){
                String currObj = maplabelName.get((tarFieldList[i].split(':')[0]).trim());
                //System.assertEquals(null,'x'+currObj+'y');
                //System.assertEquals(null,'x'+currObj.trim()+'y');
                Map <String, Schema.SObjectField> tarfieldMap = gd.get(currObj.trim()).getDescribe().fields.getMap();
                //System.assertEquals(tarfieldMap,null);
                Mapping__c instance = new Mapping__c();
                instance.Name=stgAPIName;
                String currentLabel = (stgFieldList[i].split(':')[1]).trim();
                for(String key : stgfieldMap.KeySet())
                {
                    if(currentLabel  == stgfieldMap.get(key).getDescribe().getLabel()){
                        instance.Stage_Object_Field_Name__c= stgfieldMap.get(key).getDescribe().getName();
                    }
                }
                instance.Target_Object__c= maplabelName.get((tarFieldList[i].split(':')[0]).trim());
                instance.Sequence__c=maplabelSequence.get((tarFieldList[i].split(':')[0]).trim());
                for(String key : tarfieldMap.KeySet())
                {
                    if((tarFieldList[i].split(':')[1]).trim().contains(tarfieldMap.get(key).getDescribe().getLabel())){
                        instance.Target_Object_Field_Name__c= tarfieldMap.get(key).getDescribe().getName();
                    }
                }
                listOfMapping.add(instance);
            }
        }
        database.insert(listOfMapping);
    }
    @RemoteAction
    global static void confirmObjects(String stgAPIName,List<String> objList){
    Map<String,List<String>> mapObjField = new Map<String,List<String>>();
    List<String> stgflds = new List<String>();
    for(String iter:objList){
        List<String> fields = new List<String>();
        String ObjAPI= iter.substring(iter.indexOf('(')+1,iter.indexOf(')'));
        fields.add('uni'+ObjAPI);
        stgflds.add('uni'+ObjAPI);
        mapObjField.put(ObjAPI,fields);
    }
    mapObjField.put(stgAPIName,stgflds);
    System.debug(mapObjField);
    MetadataService.MetadataPort service = MetadataServicesCreation.createService();
    Database.executeBatch(new BatchCreateMetadataSoap_Unique(mapObjField,service));
    
    }    
}